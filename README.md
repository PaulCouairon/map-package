# Map - Python for a Data-Scientist
Based on data sets ranging from 2003 to 2017 and listing hazardous waste emissions 
and the responsible facilities, the package enables data to be graphically presented 
on an interactive map. First we create two maps (years 2003 and 2017). 
The diameter of the circle associated with each emission zone is proportional to the amount of waste produced.

Then we extend this process to other datasets. Importation of data is automated for years between 2003 and 2017 
and the associated maps are successively generated and saved in PNG format. 
Finally, the fifteen maps obtained are gathered to create a short video (GIF).

![header.gif](header.gif)

## Dependencies
To generate the PNG frames, we need to install the following modules : 
1.  Pyproj
2.  Folium
3.  Selenium
4.  PIL

`pip install pyproj` 
`pip install folium` 
`pip install selenium` 
`pip install pil`

Eventually, the GIF was generated using **ffmpeg**. 