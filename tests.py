# -*- coding: utf-8 -*-
"""
Created on Mon Nov 18 19:53:13 2019

@author: vince
"""

""" Unit test for Map.py """

import unittest
import numpy as np
import matplotlib.pyplot as plt
from Map import *

class TestMy_map(unittest.TestCase):
    "Tests for submodule My_map"

    def test_bad_cases(self):
        with self.assertRaises(TypeError):
            data1 = []
            data2 = []
            My_map(data1, data2)
        with self.assertRaises(ValueError):
            data1 = np.random.rand(10, 3, 2)
            data2 = np.random.rand(10, 3, 2)
            My_map(data1, data2)
        with self.assertRaises(ValueError):
            data1 = np.random.rand(10, 3)
            data1 = np.random.rand(10, 3)
            My_map(data1, data2)


if __name__ == '__main__':
    unittest.main()